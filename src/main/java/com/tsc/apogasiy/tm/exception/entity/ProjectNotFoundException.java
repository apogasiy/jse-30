package com.tsc.apogasiy.tm.exception.entity;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project was not found!");
    }

}
