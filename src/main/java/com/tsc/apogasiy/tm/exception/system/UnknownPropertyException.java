package com.tsc.apogasiy.tm.exception.system;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class UnknownPropertyException extends AbstractException {

    public UnknownPropertyException() {
        super("Error! Unknown property!");
    }

}
