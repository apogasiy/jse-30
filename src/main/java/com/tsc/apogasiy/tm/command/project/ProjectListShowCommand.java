package com.tsc.apogasiy.tm.command.project;

import com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import com.tsc.apogasiy.tm.enumerated.Sort;
import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public @NotNull String getCommand() {
        return "project-list";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Project> projects;
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        if (!Optional.ofNullable(sort).isPresent() || sort.isEmpty())
            projects = serviceLocator.getProjectService().findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortType.getComparator());
        }
        for (@NotNull final Project project : projects) showProject(project);
    }

}
